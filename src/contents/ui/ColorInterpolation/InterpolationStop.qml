// SPDX-License-Identifier: LGPL-3.0
// SPDX-FileCopyrightText: 2012 Jérémie Astori <jeremie@astori.fr>
import QtQuick 2.15

Item {
    property real position: 0.0
    property color color: "black"
}
